<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xpath-default-namespace="http://www.w3.org/1999/xhtml"
  version="3.0">
  
  <xsl:template match="/">
    <xsl:variable name="step" select="." as="document-node()"/>  
    <xsl:variable name="step" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step" mode="he2uml"/>
      </xsl:document>
    </xsl:variable>
    <!--<xsl:variable name="step" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step" mode="uml2json"/>
      </xsl:document>
    </xsl:variable>-->
    <xsl:sequence select="$step"/>
  </xsl:template>
  
  <!--===================================-->
  <!--he2uml-->
  <!--===================================-->
  
  <!--Modele UML-->
  <!--<document>
    <id></id>
    <documentType></documentType>
    <editorialDate></editorialDate>
    <title></title>
    <simpleTitle></simpleTitle>
    <introduction></introduction>
    <faq></faq>
    <codeReference></codeReference>
    <caseLawReference/>
    <seeAlsoLinks></seeAlsoLinks>
  </document>
   - asset-hyperlink => liens PDF (pas utilisé aujourd'hui)
   - embedded-entry-block => figure / encandré / lien vidéo
   - embedded-asset-block => tableaux et image (jpg)
   
   symbol = string de moins de 250 char
   enabledNodeTypes => par défaut accepte text et paragraph (+ document qui est un wrapper)
  -->
  
  
  <xsl:template match="/" mode="he2uml">
    <document>
      <id><xsl:value-of select="html/head/meta[@property = 'dc:identifier'][not(@role)]"/></id>
      <documentType>fiche</documentType>
      <!--fixme format de date-->
      <editorialDate><xsl:value-of select="html/head/meta[@property = 'dc:date'][not(@role)]"/></editorialDate>
      <xsl:apply-templates mode="#current"/>
    </document>
  </xsl:template>
  
  <xsl:template match="body | head | article | html" mode="he2uml">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="head/*" mode="he2uml"/>
    
  <xsl:template match="h1[@class = 'els-content-title']" mode="he2uml">
    <title>
      <xsl:apply-templates mode="#current"/>
    </title>
    <simpleTitle> <!--cf. head/title qui contient déjà le titre sans enrichissements-->
      <xsl:value-of select="."/>
    </simpleTitle>
  </xsl:template>
  
  <!--schematron : vérifier que introduction est toujours au début de la fiche OASIS-->
  <xsl:template match="div[@class = 'els-block definition']" mode="he2uml">
    <!--FIXME : ne sont autorisés que les <paragraph/> sous <introduction/>, check data ?-->
    <introduction>
      <xsl:apply-templates mode="#current"/>
    </introduction>
  </xsl:template>
  
  <xsl:template match="p" mode="he2uml">
    <paragraph>
      <xsl:apply-templates mode="#current"/>
    </paragraph>
  </xsl:template>
  
  <xsl:template match="nav[@class='renvois-textes']" mode="he2uml">
    <codeReference>
      <xsl:apply-templates mode="#current"/>
    </codeReference>
  </xsl:template>
  
  <xsl:template match="nav[@class='renvois-decisions']" mode="he2uml">
    <caseLawReference>
      <xsl:apply-templates mode="#current"/>
    </caseLawReference>
  </xsl:template>
  
  <!--FIXME #1 : youness voit avec Guillaume si c'est bien des "voir aussi" => NON on les ignore-->
  <!--FIXME #2 : très typé => tableaux de liens (sans ul/li) ou Rich => autorise les listes comme les autres-->
  <xsl:template match="nav[@class='renvois-oasis']" mode="he2uml">
    <!--<seeAlsoLinks>
      <xsl:apply-templates mode="#current"/>
    </seeAlsoLinks>-->
  </xsl:template>
  
  <xsl:template match="ul[@class='els-link-list']/li/cite" mode="he2uml">
    <paragraph>
      <xsl:apply-templates mode="#current"/>
    </paragraph>
  </xsl:template>
  
  <xsl:template match="ul" mode="he2uml">
    <unordered-list>
      <xsl:apply-templates mode="#current"/>
    </unordered-list>
  </xsl:template>
  
  <xsl:template match="a[@href]" mode="he2uml">
    <hyperlink uri="https://redirect.els.eu/{lower-case(@data-dalloz-type)}/ctra/l6411-1?hulkId={@href}">
      <xsl:apply-templates mode="#current"/>
    </hyperlink>
  </xsl:template>
  
  <xsl:template match="li" mode="he2uml">
    <list-item>
      <xsl:apply-templates mode="#current"/>
    </list-item>
  </xsl:template>
  
  <xsl:template match="main" mode="he2uml">
    <body>
      <xsl:apply-templates mode="#current"/>
    </body>
  </xsl:template>
  
  <!--FIXME : on supprime la biblio pour GATEWAY ?-->
  <xsl:template match="section[@class = 'biblio']" mode="he2uml"/>
  
  <xsl:template match="*[matches(@class, 'els-heading-\d')]" mode="he2uml">
    <xsl:variable name="level" select="replace(@class, 'els-heading-', '')" as="xs:string"/>
    <xsl:element name="heading-{$level}">
      <!--fixme : @data-els-num => sera recalculé en css côté front-->
      <xsl:apply-templates mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="img[not(ancestor::figure)]" mode="he2uml">
    <!--1 on crée un asset avec l'image (on peut lui donner un id sinon il le renvoi lors de l'appel API)-->
    <asset id="{@id}" filename="@src">binaire de l'image ici - à supprimer du flux du doc</asset>
    <!--2 on créer le lien vers l'image-->
    <embedded-asset-block idref="{@id}"/>
  </xsl:template>
  
  <xsl:template match="figure" mode="he2uml">
    <!--1 on crée les assets avec les images (on peut lui donner un id sinon il le renvoi lors de l'appel API)-->
    <asset id="{@id}" filename="@src">binaire de l'image ici - à supprimer du flux du doc</asset>
    <asset id="{@id}" filename="@src">binaire de l'image ici - à supprimer du flux du doc</asset>
    <!--2 on créer la figure en tant qu'entry-->
    <entry id="{@id}" type="figure">
      <type></type>
      <caption></caption>
      <source></source>
      <images>
        <image></image>
      </images>
    </entry>
    <!--3 on créer le lien vers l'image-->4
    <embedded-entry-block idref="{@id}"/>
  </xsl:template>
  
  <xsl:template match="strong | sup | sub | em | br" mode="he2uml">
    <xsl:element name="{local-name()}">
      <xsl:apply-templates mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="*" mode="he2uml" priority="-1">
    <xsl:message>UNMATCHED <xsl:value-of select="local-name()"/> element</xsl:message>
    <xsl:next-match/>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="he2uml" priority="-2">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--===================================-->
  <!--uml2json-->
  <!--===================================-->
  
  <xsl:template match="*" mode="uml2json" priority="-1">
    <xsl:message>UNMATCHED <xsl:value-of select="local-name()"/> element</xsl:message>
    <xsl:next-match/>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="uml2json" priority="-2">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>